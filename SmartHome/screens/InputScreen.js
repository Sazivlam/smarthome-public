import React, { useState } from 'react';
import colors from '../constants/colors';
import { View, TextInput, StyleSheet, Modal, Picker, Alert, Text } from 'react-native';
import { Button } from 'react-native-elements';


const InputScreen = props => {

    const firstDeviceTypeValue = "powerOutlet";

    const [deviceName, setDeviceName] = useState('');
    const [ipAddress, setIpAddress] = useState('');
    const [deviceType, setDeviceType] = useState(firstDeviceTypeValue);

    const deviceNameInputHandler = (enteredText) => {
        setDeviceName(enteredText);
    };

    const ipAddressInputHandler = (enteredText) => {
        setIpAddress(enteredText);
    };

    const deviceTypeInputHandler = (enteredText) => {
        setDeviceType(enteredText);
    };

    const addDeviceHandler = () => {
        if (deviceName.length === 0) {
            Alert.alert(
                'Device name could not be empty',
                'Device name should be at least 1 character long',
                [{ text: 'Okay', style: 'destructive' }]
            );
            return;
        }
        if (ipAddress.length === 0) {
            Alert.alert(
                'IP address could not be empty',
                'Enter a valid IP address',
                [{ text: 'Okay', style: 'destructive' }]
            );
            return;
        }
        if (deviceType.length === 0) {
            Alert.alert(
                'Device type could not be empty',
                'Select a new device type',
                [{ text: 'Okay', style: 'destructive' }]
            );
            return;
        }
        if(!props.checkIfUnique(ipAddress)){
            Alert.alert(
                'Device with this IP address is already added',
                'Enter another IP address or check added devices list',
                [{ text: 'Okay', style: 'destructive' }]
            );
            return;
        }
        props.onAddDevice(deviceName, ipAddress, deviceType);
        setDeviceName('');
        setIpAddress('');
        setDeviceType(firstDeviceTypeValue);
    };

    return (
        <Modal animationType="slide">
            <View style={styles.inputContainer}>
                <Text style={styles.mainLabel}>Enter new device data</Text>
                <TextInput
                    placeholder="Device name"
                    style={styles.input}
                    onChangeText={deviceNameInputHandler}
                    value={deviceName}>
                </TextInput>
                <TextInput
                    label="Device IP address"
                    placeholder="Device IP address"
                    style={styles.input}
                    onChangeText={ipAddressInputHandler}
                    value={ipAddress}>
                </TextInput>

                <View style={styles.input}>
                    <Picker
                        selectedValue={deviceType}
                        onValueChange={deviceTypeInputHandler}
                    >
                        <Picker.Item label="Power Outlet" value={firstDeviceTypeValue} />
                        <Picker.Item label="Lamp" value="lamp" />
                    </Picker>
                </View>
                <View style={styles.buttonContainer}>
                    <View style={styles.button}>
                        <Button
                            title="ADD"
                            buttonStyle={{ backgroundColor: colors.primary }}
                            onPress={addDeviceHandler} />
                    </View>
                    <View style={styles.button}>
                        <Button
                            title="CANCEL"
                            buttonStyle={{ backgroundColor: colors.cancel }}
                            onPress={props.onCancel} />
                    </View>
                </View>
            </View>
        </Modal>
    );
};


const styles = StyleSheet.create({
    inputContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    input: {
        fontSize: 16,
        width: '80%',
        borderColor: colors.borderColor,
        borderWidth: 1,
        padding: 10,
        marginBottom: 20
    },
    buttonContainer: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        width: '60%'
    },
    button: {
        width: '40%'
    },
    mainLabel: {
        fontSize: 18,
        paddingVertical: 20
    }
});

export default InputScreen;