import React from 'react';
import {
    View,
    Text,
    StyleSheet,
    TouchableHighlight
} from 'react-native';

import { Button, Icon } from 'react-native-elements';
import { SwipeListView } from 'react-native-swipe-list-view';
import colors from '../constants/colors';
import Card from '../components/Card';

const axios = require('axios');

const HomeScreen = props => {

    const switchState = (ipAddress, state) => {
        let value = '';

        if (state) {
            value = 'ON';
        } else {
            value = 'OFF';
        }

        axios.get('http://' + ipAddress + '/0/' + value)
            .then(function (response) {
            })
            .catch(function (error) {
            })
            .finally(function () {
            });
    };

    return (
        <View>
            <View style={styles.listContainer}>
                <SwipeListView
                    keyExtractor={(item) => item.ipAddress}
                    data={props.devices}
                    renderItem={itemData => (
                        <Card style={styles.device}>
                            <View style={styles.deviceNameContainer}>
                                <Text style={styles.deviceName}>{itemData.item.deviceName}</Text>
                            </View>
                            <Text>IP address: {itemData.item.ipAddress}</Text>
                            <Text>Type: {itemData.item.deviceType}</Text>
                            <View style={styles.cardButtonsContainer}>
                                <View style={styles.cardButton}>
                                    <Button
                                        title="Turn ON"
                                        buttonStyle={{ backgroundColor: colors.primary }}
                                        onPress={() => switchState(itemData.item.ipAddress, true)} />
                                </View>
                                <View style={styles.cardButton}>
                                    <Button
                                        title="Turn OFF"
                                        buttonStyle={{ backgroundColor: colors.cancel }}
                                        onPress={() => switchState(itemData.item.ipAddress, false)} />
                                </View>
                            </View>
                        </Card>
                    )}
                    renderHiddenItem={itemData => (
                        <View style={styles.rowBack}>
                            <TouchableHighlight style={styles.hiddenLeft}>
                                <Icon
                                    size={42}
                                    iconStyle={styles.hiddenIcon}
                                    color='white'
                                    name='check-circle' />
                            </TouchableHighlight>
                            <TouchableHighlight style={styles.hiddenRight} onPress={props.onDelete.bind(this, itemData.item.ipAddress)}>
                                <Icon
                                    size={42}
                                    iconStyle={styles.hiddenIcon}
                                    color='white'
                                    name='delete' />
                            </TouchableHighlight>
                        </View>
                    )}
                    leftOpenValue={72}
                    rightOpenValue={-72}
                />
            </View>
        </View>
    );

};

const styles = StyleSheet.create({
    buttonContainer: {
        margin: 25
    },
    listContainer: {
        height: "85%",
        marginVertical: 30,
    },
    device: {
        padding: 10,
        marginBottom: 20,
        borderTopWidth: 1,
        borderBottomWidth: 1,
        borderColor: colors.borderColor,
        marginHorizontal: 5
    },
    deviceNameContainer: {
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'center',
        marginBottom: 5
    },
    deviceBadge: {
        alignItems: 'center'
    },
    deviceName: {
        fontSize: 18,
    },
    devices: {
        margin: 30,
        justifyContent: 'space-around'
    },
    cardButtonsContainer: {
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'space-around',
        marginTop: 10
    },
    cardButton: {
        width: 150
    },
    rowBack: {
        alignItems: 'center',
        backgroundColor: '#DDD',
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 20,
    },
    hiddenLeft: {
        backgroundColor: colors.primary,
        height: '100%',
        width: '50%',
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        borderTopWidth: 1,
        borderBottomWidth: 1,
        borderColor: colors.borderColor,
    },
    hiddenRight: {
        backgroundColor: colors.cancel,
        height: '100%',
        width: '50%',
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
        borderTopWidth: 1,
        borderBottomWidth: 1,
        borderColor: colors.borderColor,
    },
    hiddenText: {
        color: "white",
        width: 100,
        fontSize: 18,
        fontWeight: 'bold',
        paddingHorizontal: 15
    },
    hiddenIcon: {
        paddingHorizontal: 15
    }
});

export default HomeScreen;