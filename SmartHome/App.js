import React, { useState } from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { Header, Button, Icon } from 'react-native-elements';

import HomeScreen from './screens/HomeScreen';
import InputScreen from './screens/InputScreen';

export default function App() {
  const [devices, setDevices] = useState([]);
  const [isInputScreenOpen, setIsInputScreenOpen] = useState(false);

  const addDeviceHandler = (deviceName, ipAddress, deviceType) => {
    setDevices(currentGoals => [
      ...currentGoals,
      { deviceName: deviceName, ipAddress: ipAddress, deviceType: deviceType }
    ]);
    setIsInputScreenOpen(false);
  };

  const removeDeviceHandler = ipAddress => {
    setDevices(currentDevoces => {
      return currentDevoces.filter((device) => device.ipAddress !== ipAddress);
    });
  }

  const checkIfUniqueHandler = ipAddress => {
    if (devices.some(e => e.ipAddress === ipAddress)) {
      return false;
    }else {
      return true;
    }
  }

  const openInputScreen = () => {
    setIsInputScreenOpen(true);
  };

  const closeInputScreen = () => {
    setIsInputScreenOpen(false);
  };

  let content = <View style={styles.noDeviceMessageContainer}>
    <Text>No devices are added. Click on the plus icon, to add a new device</Text>
  </View>
    ;

  if (devices.length > 0) {
    content = <HomeScreen devices={devices} onDelete={removeDeviceHandler} />;
  }

  if (isInputScreenOpen) {
    content = 
    <InputScreen 
    checkIfUnique={checkIfUniqueHandler}
    onAddDevice={addDeviceHandler} 
    onCancel={closeInputScreen} />
  }

  return (
    <View style={styles.screen}>
      <Header
        leftComponent={{ icon: 'menu', color: '#fff' }}
        centerComponent={{ text: 'SmartHome App', style: { fontSize: 20, color: '#fff' } }}
        rightComponent={
          <Button
            icon={
              <Icon
                name="add"
                color="white"
              />
            }
            onPress={openInputScreen}
          />}
      />
      {content}
    </View>
  );
}

const styles = StyleSheet.create({
  noDeviceMessageContainer: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'center',
    padding: 40
  },
  screen: {
    flex: 1
  },
});
