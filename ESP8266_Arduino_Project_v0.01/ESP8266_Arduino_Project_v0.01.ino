#include <ESP8266WiFi.h>
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <WiFiManager.h>         // https://github.com/tzapu/WiFiManager
#include <DoubleResetDetector.h>

#define DRD_TIMEOUT 10
#define DRD_ADDRESS 0

DoubleResetDetector drd(DRD_TIMEOUT, DRD_ADDRESS);

// Set web server port number to 80
WiFiServer server(80);

// Variable to store the HTTP request
String header;

// Auxiliar variables to store the current output state
String relayState = "OFF";

// Assign output variables to GPIO pins
const int RELAY = 0;

bool initialConfig = false;

void setup() {
  Serial.begin(115200);
  
  // Initialize the output variables as outputs
  pinMode(RELAY, OUTPUT);
  
  // Set outputs to HIGH(OFF)
  digitalWrite(RELAY, HIGH);

 if (WiFi.SSID()==""){
    Serial.println("We haven't got any access point credentials, so get them now");   
    initialConfig = true;
  }
  if (drd.detectDoubleReset()) {
    Serial.println("Double Reset Detected");
    initialConfig = true;
  }
  if (initialConfig) {
    Serial.println("Starting configuration portal.");
    //Local intialization. Once its business is done, there is no need to keep it around
    WiFiManager wifiManager;

    //sets timeout in seconds until configuration portal gets turned off.
    //If not specified device will remain in configuration mode until
    //switched off via webserver or device is restarted.
    //wifiManager.setConfigPortalTimeout(600);

    //it starts an access point 
    //and goes into a blocking loop awaiting configuration
    if (!wifiManager.startConfigPortal()) {
      Serial.println("Not connected to WiFi but continuing anyway.");
    } else {
      //if you get here you have connected to the WiFi
      Serial.println("connected...yeey :)");
    }
    
    ESP.reset(); // This is a bit crude. For some unknown reason webserver can only be started once per boot up 
    // so resetting the device allows to go back into config mode again when it reboots.
    delay(5000);
  }


WiFi.mode(WIFI_STA);
  

//  if(drd.detectDoubleReset()){
//                WiFi.disconnect(true);
//            delay(2000);
//            ESP.reset();
//  }
//
//  // WiFiManager
//  // Local intialization. Once its business is done, there is no need to keep it around
//  WiFiManager wifiManager;
  
  // Uncomment and run it once, if you want to erase all the stored information
  //wifiManager.resetSettings();
  
  // set custom ip for portal
  //wifiManager.setAPConfig(IPAddress(10,0,1,1), IPAddress(10,0,1,1), IPAddress(255,255,255,0));

  // fetches ssid and pass from eeprom and tries to connect
  // if it does not connect it starts an access point with the specified name
  // here  "AutoConnectAP"
  // and goes into a blocking loop awaiting configuration
//  wifiManager.autoConnect("AutoConnectAP");
  // or use this for auto generated name ESP + ChipID
  //wifiManager.autoConnect();
  
  // if you get here you have connected to the WiFi
  Serial.println("Connected.");
  
  server.begin();
}

String getPage(){
  String page = "<html charset=UTF-8><head>";
  page += "<meta charset='utf-8'>";
  page += "<meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no'>";
  page += "<script src='https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js'></script><script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js'></script>";
  page += "<link rel='stylesheet' href='https://bootswatch.com/3/superhero/bootstrap.min.css'>";
  page += "<title>ESP8266 Demo - www.projetsdiy.fr</title></head><body>";
  page += "<div class='container-fluid'>";
  page += "<div class='row'>";
  page += "<div class='col-md-12'>";
  page += "<h1 class ='text-center'>Demo Webserver ESP8266 + Bootstrap</h1>";
  page += "<div class='col'><h2 class ='text-center'>Relay ";
 if (relayState=="OFF") {
              page += "<span class='badge' style='font-size:16px; font-weight: 1000; background-color: #d9534f;'>";
              page += relayState;
              page += "</span></h2></div>";
              page += "<div class='col-md-6 col-md-offset-3 col-sm-12'><a href=\"/0/ON\"><button style='width: 100%; margin-top: 20px;' class='btn btn-success btn-lg'>Turn ON</button></a></div>";
            } else {
              page += "<span class='badge' style='font-size:16px; font-weight: 1000; background-color: #5cb85c;'>";
              page += relayState;
              page += "</span></h2></div>";
              page += "<div class='col-md-6 col-md-offset-3 col-sm-12'><a href=\"/0/OFF\"><button style='width: 100%; margin-top: 20px;' class='btn btn-danger btn-lg'>Turn OFF</button></a></div>";
            } 

  page += "</div>";
  page += "</div></div></div>";
  page += "</body></html>";
  return page;
}

void loop(){
  WiFiClient client = server.available();   // Listen for incoming clients

  if (client) {                             // If a new client connects,
    Serial.println("New Client.");          // print a message out in the serial port
    String currentLine = "";                // make a String to hold incoming data from the client
       
    while (client.connected()) {            // loop while the client's connected
      if (client.available()) {             // if there's bytes to read from the client,
        char c = client.read();             // read a byte, then
        Serial.write(c);                    // print it out the serial monitor
        header += c;
        if (c == '\n') {                    // if the byte is a newline character
          // if the current line is blank, you got two newline characters in a row.
          // that's the end of the client HTTP request, so send a response:
          if (currentLine.length() == 0) {
            // HTTP headers always start with a response code (e.g. HTTP/1.1 200 OK)
            // and a content-type so the client knows what's coming, then a blank line:
            client.println("HTTP/1.1 200 OK");
            client.println("Content-type:text/html");
            client.println("Connection: close");
            client.println();
            
            // turns the GPIOs on and off
            if (header.indexOf("GET /0/ON") >= 0) {
              Serial.println("Relay on");
              relayState = "ON";
              digitalWrite(RELAY, LOW);
            } else if (header.indexOf("GET /0/OFF") >= 0) {
              Serial.println("Relay off");
              relayState = "OFF";
              digitalWrite(RELAY, HIGH);
            }
            
            client.println(getPage());
            
            // The HTTP response ends with another blank line
            client.println();
            // Break out of the while loop
            break;
          } else { // if you got a newline, then clear currentLine
            currentLine = "";
          }
        } else if (c != '\r') {  // if you got anything else but a carriage return character,
          currentLine += c;      // add it to the end of the currentLine
        }
      }
    }
    // Clear the header variable
    header = "";
    // Close the connection
    client.stop();
    Serial.println("Client disconnected.");
    Serial.println("");
  }

  drd.loop();
}
